﻿using Core.Entities;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Shared
{
    public class Helper
    {
        public Users CreatUser(UserModel user)
        {
            var hash = new PasswordHash(user.Password);
            byte[] hashBytes = hash.ToArray();
            var newUser = new Users
            {
                Username = user.Username,
                Password = hashBytes,
                UserStatusId = user.UserStatusId,
                Email = user.Email,
                Phone = user.Phone,
                Fullname = user.Fullname,
                Address = user.Address,
                PresenterId = user.PresenterId,
                BranchId = user.BranchId,
                GuidId = user.GuidId,
                UserRoles = user.UserRoles,
                IsDelete = user.IsDelete,
                CreatedUtcTime = user.CreatedUtcTime,
                ModifiedUtcDate = user.ModifiedUtcDate,
            };
            return newUser;
        }
    }
}
