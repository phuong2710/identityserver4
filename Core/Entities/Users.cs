﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class Users
    {
        public long UserId { get; set; }
        public string Username { get; set; }
        public byte[] Password { get; set; }
        public int UserStatusId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Fullname { get; set; }
        public string Address { get; set; }
        public long PresenterId { get; set; }
        public long BranchId { get; set; }
        public int GuidId { get; set; }
        public string UserRoles { get; set; }
        public int IsDelete { get; set; }
        public DateTime CreatedUtcTime { get; set; }
        public DateTime ModifiedUtcDate { get; set; }
    }
}
