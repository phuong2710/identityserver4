﻿using Core.Entities;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Services

{
    public interface IUserService
    {
        IEnumerable<Users> GetAll();
        Users Get(long userId);
        void Create(Users user);
        void Update(int id,Users user);
        void Delete(int id);

        Task<Users> VerifyPassword(string username,string password);
        

    }
}
