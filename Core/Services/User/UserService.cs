﻿using Core.Entities;
using Core.Exceptions;
using Core.Models;
using Core.Shared;
using Dapper;
using MySqlConnector;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Services
{
    public class UserService : IUserService
    {
        private readonly string connectionString;

        public UserService(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public IEnumerable<Users> GetAll()
        {
            IEnumerable<Users> user = null;
            using (var conn = new MySqlConnection(connectionString))
            {
                user = conn.Query<Users>("select * from T_User");
            }
            return user;
        }
        public void Create(Users user)
        {
            using (var connection = new MySqlConnection(connectionString))
            {
                string sql = "INSERT INTO T_User(Username,Password,UserStatusId,Email,Phone,Fullname,Address,PresenterId,BranchId,GuidId,UserRoles,IsDelete,CreatedUtcTime,ModifiedUtcDate) " +
                    "VALUES(@Username, @Password, @UserStatusId, @Email, @Phone, @Fullname, @Address, @PresenterId, @BranchId, @GuidId, @UserRoles, @IsDelete, @CreatedUtcTime, @ModifiedUtcDate)";
                connection.Execute(sql, new
                {
                    user.Username,
                    user.Password,
                    user.UserStatusId,
                    user.Email,
                    user.Phone,
                    user.Fullname,
                    user.Address,
                    user.PresenterId,
                    user.BranchId,
                    user.GuidId,
                    user.UserRoles,
                    user.IsDelete,
                    user.CreatedUtcTime,
                    user.ModifiedUtcDate
                });
            }

        }
        public void Update(int id,Users user)
        {
            using (var connection = new MySqlConnection(connectionString))
            {
                string sql = "UPDATE T_User SET Username = @Username, Password = @Password, UserStatusId = @UserStatusId, " +
                    "Email = @Email, Phone = @Phone, Fullname = @Fullname, Address = @Address, PresenterId = @PresenterId, BranchId = @BranchId," +
                    " GuidId = @GuidId, UserRoles = @UserRoles, IsDelete = @IsDelete, CreatedUtcTime = @CreatedUtcTime, ModifiedUtcDate = @ModifiedUtcDate WHERE UserId = @Userid";
                connection.Execute(sql, new { user.Username, user.Password, user.UserStatusId, user.Email, user.Phone, user.Fullname, user.Address, user.PresenterId, user.BranchId, user.GuidId, user.UserRoles, user.IsDelete, user.CreatedUtcTime, user.ModifiedUtcDate, UserId = id });
            }
        }

        public void Delete(int DelId)
        {
            using(var connection = new MySqlConnection(connectionString))
            {
                string sql = "DELETE FROM T_User where UserId = @UserId";
                connection.Execute(sql, new { UserId = DelId });
            }
        }

        public async Task<Users> VerifyPassword(string username,string password)
        {
            using(var conn = new MySqlConnection(connectionString))
            {
                string sql = "select * from T_User where UserName = @UserName";
                var user =  conn.Query<Users>(sql, new { UserName = username }).SingleOrDefault<Users>();
                if(user == null)
                    throw new ServiceException("USER_NAME_NOT_EXISTED");
                var hash = new PasswordHash(user.Password);
                if (!hash.Verify(password))
                    throw new ServiceException("PASSWORD_INCORECTED");
                return user;
            }
        }

        public Users Get(long userId)
        {
           using(var conn = new MySqlConnection(connectionString))
            {
                try
                {
                    string sql = "select * from T_User where UserId = userId";
                    var user = conn.Query<Users>(sql, new { UserId = userId }).SingleOrDefault<Users>();
                    return user;
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Get {0}", ex.Message));
                    return null;
                }
            }
        }

        //public Users CheckUserName(string username)
        //{
        //    using (var conn = new MySqlConnection(connectionString))
        //    {
        //        string sql = "select * from T_User where UserName = @UserName";
        //        var user = conn.Query<Users>(sql, new { UserName = username }).SingleOrDefault<Users>();
        //        return user;
        //    }
        //}

        //public bool CheckPassword(Users user, string password)
        //{
        //    var hash = new PasswordHash(user.Password);
        //    if (hash.Verify(password))
        //        return true;
        //    return false;
        //}
    }
}
