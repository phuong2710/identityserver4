﻿using Auth.IdentityServer;
using Core.Entities;
using Core.Services;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServerMVC
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        
            private readonly IUserService userService;
            public ResourceOwnerPasswordValidator(IUserService userService)
            {
                this.userService = userService;
            }
            public virtual async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
            {

                try
                {
                    Users user =  await userService.VerifyPassword(context.UserName, context.Password);

                    if (user != null)
                    {
                        context.Result = new GrantValidationResult(
                           subject: user.UserId.ToString(),
                           authenticationMethod: "custom",
                           claims: Config.GetUserClaims(user));
                    }
                    else
                    {
                        context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant);
                    }
                }
                catch (Exception ex)
                {
                    Log.Fatal(ex, ex.Message);
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, ex.Message);
                }
        }

        
    }
}
