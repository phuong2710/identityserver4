﻿using Auth.IdentityServer;
using Core.Services;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServerMVC.Service
{
    public class ProfileService : IProfileService
    {
        private readonly IUserService userService;
        public ProfileService(IUserService userService)
        {
            this.userService = userService;
        }

        public  Task GetProfileDataAsync(ProfileDataRequestContext context)  //Xác nhận quyền sở hữu cho User
        {


            //ProfileDataRequest mô hình hóa User Claim, là phương tiện trả về các Claim
            //Subject: Các mô hình Model User (ClaimsPrincipal)
            //Client: Các Client mà Claims đang được yêu cầu
            //RequestClaimTypes: Bộ sưu tập các Claims đang được yêu cầu
            //IssuedClaims: Danh sách Claim được trả lại, triển khai trong IProfileService

            //var sub = context.Subject.FindFirst("sub")?.Value;
            //if(sub != null)
            //{
            //    var user = await myUserService.CheckUserName(sub);
            //    var cp = await getClaim(user);
            //    var claim = cp.Claims;

            //}
            try
            {
                //depending on the scope accessing the user data.
                if (!string.IsNullOrEmpty(context.Subject.Identity.Name))
                {
                    //get user from db (in my case this is by email)
                    var user = userService.Get(Convert.ToInt32(context.Subject.Identity.Name));

                    if (user != null)
                    {
                        var claims = Config.GetUserClaims(user);

                        //set issued claims to return
                        context.IssuedClaims = claims.Where(x => context.RequestedClaimTypes.Contains(x.Type)).ToList();
                    }
                }
                else
                {
                    //get subject from context (this was set ResourceOwnerPasswordValidator.ValidateAsync),
                    //where and subject was set to my user id.
                    var userId = context.Subject.Claims.FirstOrDefault(x => x.Type == "sub");

                    if (!string.IsNullOrEmpty(userId?.Value) && long.Parse(userId.Value) > 0)
                    {
                        //get user from db (find user by user id)
                        var user = userService.Get(int.Parse(userId.Value));

                        // issue the claims for the user
                        if (user != null)
                        {
                            var claims = Config.GetUserClaims(user);

                            context.IssuedClaims = claims.ToList();//.Where(x => context.RequestedClaimTypes.Contains(x.Type)).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            return Task.CompletedTask;
        }

        public Task IsActiveAsync(IsActiveContext context)    //Cho biết User có được lấy token ko
        {
            try
            {
                //get subject from context (set in ResourceOwnerPasswordValidator.ValidateAsync),
                var userId = context.Subject.Claims.FirstOrDefault(x => x.Type == "user_id");

                if (!string.IsNullOrEmpty(userId?.Value) && int.Parse(userId.Value) > 0)
                {
                    var user = userService.Get(int.Parse(userId.Value));

                    if (user != null)
                    {
                        context.IsActive = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            return Task.CompletedTask;
        }
    }
}
