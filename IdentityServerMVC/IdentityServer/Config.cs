﻿using Core.Entities;
using IdentityModel;
using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Auth.IdentityServer
{
    public class Config
    {
        //public static IEnumerable<IdentityResource> GetIdentityResources()
        //{
        //    return new List<IdentityResource>
        //    {
        //        new IdentityResources.OpenId(),
        //        new IdentityResources.Profile(),
        //    };
        //}
        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope("api1","My Api")
            };
        }
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("main_api","MY MAIN API")
                {
                    Scopes = { "api1" }
                }
            };
        }
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "ro.client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    //RedirectUris = new List<string>(clientAppRedirectUri),
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    RequireClientSecret = true,
                    AllowedScopes = {
                        "main_api",
                        "openid",
                        "profile",
                        "offline_access",
                    },
                    AllowOfflineAccess = true,
                    Enabled = true,
                    AllowAccessTokensViaBrowser = true,
                    //AllowedCorsOrigins = new  List<string>(clientAppRedirectUri),
                    AccessTokenType = AccessTokenType.Jwt,
                    IdentityTokenLifetime = 3000,
                    AccessTokenLifetime = 3600*24*30,
                    AuthorizationCodeLifetime = 300
                }
            };
        }
        public static Claim[] GetUserClaims(Users user)
        {
            return new Claim[]
            {
                new Claim("user_id", user.UserId.ToString()),
                new Claim(JwtClaimTypes.PhoneNumber, user.Phone?? string.Empty),
                //new Claim("user_type", JsonConvert.SerializeObject(userType)),
                ////roles
                new Claim("permissions", user.UserRoles??string.Empty),
            };
        }
    }
    
}
   

