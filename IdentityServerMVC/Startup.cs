﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Auth.IdentityServer;
using Core.Services;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using IdentityServerMVC.Service;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace IdentityServerMVC
{
    public class Startup
    {
        public IConfiguration Configuration { get; private set; }
        public IWebHostEnvironment Environment { get; private set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //Đăng kí các dịch vụ
            var builder = new ConfigurationBuilder().SetBasePath(System.IO.Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            var config = builder.Build();
            services.AddTransient<IUserService>(f => new UserService(config["ConnectionString:UserManagement"]));

            //var cert = new X509Certificate2(Path.Combine(Environment.ContentRootPath, "IdentityServer", "idsrv4.pfx"), "S*u9XdF)`E2+@<\"H!2Vh:(n=,<cE4,Qb");
            services.AddIdentityServer()
            .AddDeveloperSigningCredential()
            .AddOperationalStore(                     //Chưa hiểu 
                option =>
                {
                    option.EnableTokenCleanup = true;
                    option.TokenCleanupInterval = 30;
                }
                )
            .AddInMemoryApiScopes(Config.GetApiScopes())  //Khai báo tài nguyên ApiResource
            .AddInMemoryApiResources(Config.GetApiResources())
            .AddInMemoryClients(Config.GetClients());        //Khai báo thông tin máy khách
          
            
            services.AddTransient<IProfileService, ProfileService>();          //Đăng kí các gỏi Service
            services.AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>();

        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseIdentityServer();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
